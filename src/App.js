//#region PACKAGE IMPORTS
import React from 'react';
import { BrowserRouter, Redirect, Route, Switch } from 'react-router-dom';
import { Provider } from 'react-redux';
//#endregion

//#region MODULE IMPORTS
import Layout from './components/Layout';
import SpinningPokeball from './components/SpinningPokeball';
//#endregion

//#region CONFIG IMPORTS
import { useStore } from './config/store';
//#endregion

const PokemonListPage = React.lazy(() => import('./pages/PokemonList'));
const PokemonDetailPage = React.lazy(() => import('./pages/PokemonDetail'));
const MyPokemonPage = React.lazy(() => import('./pages/MyPokemon'));

function App() {
  const store = useStore();
  return (
    <Provider store={store}>
      <BrowserRouter>
        <React.Suspense fallback={<SpinningPokeball />}>
          <Layout>
            <Switch>
              <Route exact path="/" component={PokemonListPage} />
              <Route
                exact
                path="/pokemon-detail/:id"
                component={PokemonDetailPage}
              />
              <Route exact path="/my-pokemon" component={MyPokemonPage} />
              <Redirect to="/" />
            </Switch>
          </Layout>
        </React.Suspense>
      </BrowserRouter>
    </Provider>
  );
}

export default App;
