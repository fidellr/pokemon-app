//#region PACKAGE IMPORTS
import React from 'react';
//#endregion

//#region STYLESHEET IMPORTS
import './BottomSheet.scss';
//#endregion

const BottomSheet = ({ className, children }) => {
  const classes = !className ? 'sheetContainer' : `sheetContainer ${className}`;
  return <div className={classes}>{children}</div>;
};

export default BottomSheet;
