//#region PACKAGE IMPORTS
import React from 'react';
//#endregion

//#region STYLESHEET IMPORTS
import './Button.scss';
//#endregion

const Button = ({
  className,
  disabled,
  onClick,
  children,
  isRounded,
}) => {
  const classes = !className ? 'button' : `${className} button`;
  const roundedClasses = !isRounded ? '' : ' is-rounded';

  return (
    <button
      disabled={disabled}
      className={classes + roundedClasses}
      onClick={onClick}
    >
      {children}
    </button>
  );
};

export default Button;
