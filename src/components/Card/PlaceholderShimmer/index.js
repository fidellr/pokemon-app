//#region PACKAGE IMPORTS
import React from 'react';
import Card from '../index.js';
//#endregion

const PlacholderShimmer = ({ className }) => {
  return <Card className={className} isLoading />;
};

export default PlacholderShimmer;
