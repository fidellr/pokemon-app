//#region PACKAGE IMPORTS
import React from 'react';
//#endregion

//#region STYLESHEET IMPORTS
import './Card.scss';
//#endregion

const Card = ({ className, onClick, name, image, id, isLoading }) => {
  const containerClasses = `${
    !className ? 'cardContainer' : `cardContainer ${className}`
  }`;

  return (
    <div
      className={`${containerClasses} ${isLoading ? ' is-loading' : ''}`}
      onClick={onClick}
    >
      <div className="cardInfo">
        <p className="cardName">{name}</p>
        <p className="cardId">{!id ? '' : `#${id}`}</p>
      </div>
      <div
        className="cardImage"
        style={{ backgroundImage: `url('${image}')` }}
      />
    </div>
  );
};

export default Card;
