//#region PACKAGE IMPORTS
import React from 'react';
//#endregion

//#region STYLESHEET IMPORTS
import './Chip.scss';
//#endregion

const Chip = ({ className, text, backgroundColor, textColor }) => {
    const classes = !className ? "chipContainer" : `chipContainer ${className}`
  return (
    <div
      className={classes}
      style={{ backgroundColor, color: textColor }}
    >
      <p className="chipText">{text}</p>
    </div>
  );
};

export default Chip;
