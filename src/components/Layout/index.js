//#region PACKAGE IMPORTS
import React, { useEffect } from 'react';
import { useDispatch } from 'react-redux';
import { rehydrateMyPokemon } from '../../config/store/actions';
//#endregion

//#region MODULE IMPORTS
import Navbar from '../Navbar';
//#endregion

//#region STYLESHEET IMPORTS
import './Layout.scss';
//#endregion

const Layout = ({ children }) => {
  const dispatch = useDispatch();
  useEffect(() => {
    dispatch(rehydrateMyPokemon());
  }, [dispatch]);
  return (
    <div className="layoutContainer">
      <Navbar />
      <main>{children}</main>
    </div>
  );
};

export default Layout;
