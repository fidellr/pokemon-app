//#region PACKAGE IMPORTS
import React from 'react';
import { BiX } from 'react-icons/bi';
//#endregion

//#region STYLESHEET IMPORTS
import './Modal.scss';
//#endregion

const Modal = ({ className, isOpen, onClose, children }) => {
  const closeModal = (e) => {
    e.stopPropagation();
    onClose();
  };

  const divStyle = {
    display: isOpen ? 'block' : 'none',
  };

  const contentClasses = !className ? 'content' : `${className} content`;
  return (
    <div className="modal" onClick={closeModal} style={divStyle}>
      <div className="modalContent" onClick={(e) => e.stopPropagation()}>
        <div className="closeButton" onClick={closeModal}>
          <BiX />
        </div>
        <div className={contentClasses}>{children}</div>
      </div>
    </div>
  );
};

export default Modal;
