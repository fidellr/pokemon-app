//#region PACKAGE IMPORTS
import React, { useEffect, useState } from 'react';
import { useLocation, useHistory } from 'react-router-dom';
import { BiArrowBack, BiHome, BiShoppingBag } from 'react-icons/bi';
//#endregion

//#region STYLESHEET IMPORTS
import './Navbar.scss';
//#endregion

const Navbar = () => {
  //#region ROUTES
  const location = useLocation();
  const history = useHistory();
  const isListPage = location.pathname === '/';
  const isMyPokemonPage = location.pathname === '/my-pokemon';
  //#endregion

  //#region STATES
  const [scrollPosition, setScrollPosition] = useState(0);
  //#endregion

  //#region LIFECYCLES
  useEffect(() => {
    window.addEventListener('scroll', () => setScrollPosition(window.scrollY));

    return () => {
      window.removeEventListener('scroll', () =>
        setScrollPosition(window.scrollY)
      );
    };
  }, []);
  //#endregion

  //#region RENDERER
  const renderNavigationLeftButton = () => {
    if (!isListPage && !isMyPokemonPage) {
      return (
        <BiArrowBack className="leftButton" onClick={() => history.push('/')} />
      );
    } else if (!isListPage) {
      return (
        <BiHome className="leftButton" onClick={() => history.push('/')} />
      );
    }
  };

  const renderNavigationRightButton = () => {
    return (
      <BiShoppingBag
        className="rightButton"
        onClick={() => history.push('/my-pokemon')}
      />
    );
  };
  //#endregion

  const containerClasses =
    scrollPosition >= 36 ? 'navbarContainer has-boxShadow' : 'navbarContainer';
  const titleClasses = isListPage ? 'navbarTitle is-listPage' : 'navbarTitle';

  return (
    <div className={containerClasses}>
      {renderNavigationLeftButton()}
      <h3 className={titleClasses}>Pokemania</h3>
      {renderNavigationRightButton()}
    </div>
  );
};

export default Navbar;
