//#region PACKAGE IMPORTS
import React from 'react';
//#endregion

//#region STYLESHEET IMPORTS
import './SpinningPokeball.scss';
//#endregion

const SpinningPokeball = () => {
  return (
    <div className="pokeballContainer">
      <div className="pokeball" />
    </div>
  );
};

export default SpinningPokeball;
