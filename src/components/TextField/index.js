//#region PACKAGE IMPORTS
import React from 'react';
//#endregion

//#region STYLESHEET IMPORTS
import './TextField.scss';
//#endregion

const TextField = ({ name, value, label, onChange, placeholder }) => {
  return (
    <div className="textFieldContainer">
      <label className={`${name}-label`} htmlFor={name}>
        {label}
      </label>
      <input
        id={name}
        value={value}
        className={`${name}-input`}
        onChange={onChange}
        placeholder={placeholder}
      />
    </div>
  );
};

export default TextField;
