//#region MODULE IMPORTS
import { ls } from '../../utils/helpers';
import * as types from './actionTypes';
//#endregion

export const savePokemon = (data) => (dispatch) => {
  dispatch({ type: types.LOADING_MY_POKEMON });
  try {
    let savedData = ls.getItem('my_pokemon') || [];
    const isDataExist =
      savedData &&
      savedData.length &&
      savedData.filter(
        (item) => item.name === data.name && item.nickname === data.nickname
      ).length > 0;
    if (isDataExist) throw new Error(`${data.nickname} nickname is exist`);

    if (!savedData || savedData.length < 1) {
      savedData = [data];
    } else {
      savedData = [data, ...savedData];
    }

    ls.setItem('my_pokemon', savedData);
    dispatch({
      type: types.SUCCESS_SAVE_POKEMON,
      payload: {
        catchedPokemon: data,
      },
    });
  } catch (error) {
    dispatch({
      type: types.FAILED_SAVE_POKEMON,
      payload: { error: error.message },
    });
  }
};

export const removeSavedPokemonByNickname = (data) => (dispatch) => {
  const savedData = ls.getItem('my_pokemon');
  const updatedData =
    savedData &&
    savedData.reduce((all, item) => {
      if (item.nickname === data.nickname && item.name === data.name) {
        return all;
      }
      return [...all, item];
    }, []);
  ls.setItem('my_pokemon', updatedData);
  dispatch({
    type: types.SUCCESS_REMOVE_POKEMON,
    payload: {
      myPokemon: updatedData,
    },
  });
};

export const rehydrateMyPokemon = () => (dispatch) => {
  try {
    const savedData = ls.getItem('my_pokemon') || [];
    dispatch({
      type: types.SUCCESS_REHYDRATE_SAVED_POKEMON,
      payload: {
        catchedPokemons: savedData,
        error: null,
      },
    });
  } catch (error) {
    dispatch({
      type: types.FAILED_REHYDRATE_SAVED_POKEMON,
      payload: { error: error.message },
    });
  }
};

export const putErrorMessage = (errorMessage) => (dispatch) => {
  dispatch({
    type: types.SET_ERROR_MESSAGE,
    payload: {
      error: errorMessage,
    },
  });
};
