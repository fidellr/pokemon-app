//#region MODULE IMPORTS
import * as types from './actionTypes';
//#endregion

const initialState = {
  myPokemon: [],
  isLoading: false,
  error: null,
};

const myPokemonReducer = (state = initialState, { type, payload }) => {
  const actions = {
    [types.LOADING_MY_POKEMON]: () => {
      return {
        ...state,
        isLoading: true,
      };
    },
    [types.SUCCESS_SAVE_POKEMON]: () => {
      return {
        ...state,
        myPokemon: [payload.catchedPokemon, ...state.myPokemon],
        error: null,
        isLoading: false,
      };
    },
    [types.FAILED_SAVE_POKEMON]: () => {
      return {
        ...state,
        error: payload.error,
        isLoading: false,
      };
    },
    [types.SUCCESS_REMOVE_POKEMON]: () => {
      return {
        ...state,
        myPokemon: payload.myPokemon,
      };
    },
    [types.SUCCESS_REHYDRATE_SAVED_POKEMON]: () => {
      return {
        ...state,
        myPokemon: payload.catchedPokemons,
        error: null,
        isLoading: false,
      };
    },
    [types.FAILED_REHYDRATE_SAVED_POKEMON]: () => {
      return {
        ...state,
        error: payload.error,
      };
    },
    [types.SET_ERROR_MESSAGE]: () => {
      return {
        ...state,
        error: payload.error,
      };
    },
    DEFAULT: () => state,
  };

  return (actions[type] || actions.DEFAULT)();
};

export default myPokemonReducer;
