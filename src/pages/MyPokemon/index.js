//#region PACKAGE IMPORTS
import React, { useState } from 'react';
import { useHistory } from 'react-router';
import { useDispatch, useSelector } from 'react-redux';
import { BiXCircle } from 'react-icons/bi';
//#endregion

//#region MODULE IMPORTS
import Modal from '../../components/Modal';
//#endregion

//#region CONFIG IMPORTS
import { removeSavedPokemonByNickname } from '../../config/store/actions';
//#endregion

//#region STYLESHEET IMPORTS
import './MyPokemon.scss';
//#endregion

const MyPokemon = () => {
  //#region ROUTES
  const history = useHistory();
  //#endregion

  //#region REDUX
  const states = useSelector((state) => state);
  const dispatch = useDispatch();
  //#endregion

  //#region STATES
  const [isModalOpen, setIsModalOpen] = useState(false);
  const [removedCharacter, setRemovedCharacter] = useState(null);
  //#endregion

  //#region HANDLER
  const removeSavedPokemon = (character) => {
    dispatch(removeSavedPokemonByNickname(character));
  };

  const toggleModal = (character) => {
    if (!isModalOpen) {
      setRemovedCharacter(character);
      setIsModalOpen(true);
      removeSavedPokemon(character);
      return;
    }
    setIsModalOpen(false);
    setRemovedCharacter(null);
  };
  //#endregion

  //#region RENDERER
  const renderModal = () => {
    if (!removedCharacter) return;
    return (
      <Modal
        className="removedModal"
        isOpen={isModalOpen}
        onClose={() => setIsModalOpen(!isModalOpen)}
      >
        <h3>{removedCharacter.nickname} are removed!</h3>
        <p>Go catch another!</p>
      </Modal>
    );
  };

  const renderMyPokemonList = () => {
    if (!states.myPokemon || !states.myPokemon.length) {
      return (
        <h3 className="emptyCharacterListMessage">
          You have not catch anything yet, go catch it!
        </h3>
      );
    }

    return (
      <>
        <p className="savedPokemonCount">
          Saved Pokemon: <b>&nbsp;{states.myPokemon.length}</b>
        </p>
        <div className="characterListContainer">
          {states.myPokemon.map((item) => (
            <div
              key={`${item.name}-${item.nickname}`}
              className="characterContainer"
            >
              <div
                className="characterWrapper"
                onClick={() => history.push(`/pokemon-detail/${item.id}`)}
              >
                <div
                  className="characterImage"
                  style={{
                    backgroundImage: `url('${item.sprites.front_default}')`,
                  }}
                />
                <div className="characterInfo">
                  <span className="characterNickname">
                    <b>Nickname:</b> {item.nickname}
                  </span>
                  <span className="characterName">
                    <b>Character:</b> {item.name}
                  </span>
                </div>
              </div>
              <BiXCircle
                className="characterRemoveButton"
                onClick={() => toggleModal(item)}
              />
            </div>
          ))}
        </div>
      </>
    );
  };
  //#endregion

  return (
    <div className="myPokemonContainer">
      {renderMyPokemonList()}
      {renderModal()}
    </div>
  );
};

export default MyPokemon;
