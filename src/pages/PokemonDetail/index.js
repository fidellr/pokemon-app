//#region PACKAGE IMPORTS
import React, { useCallback, useEffect, useState } from 'react';
import { useParams } from 'react-router-dom';
import SwiperCore, { EffectCoverflow } from 'swiper';
import { Swiper, SwiperSlide } from 'swiper/react';
import { useDispatch, useSelector } from 'react-redux';
//#endregion

//#region MODULE IMPORTS
import BottomSheet from '../../components/BottomSheet';
import Chip from '../../components/Chip';
import Button from '../../components/Button';
import Modal from '../../components/Modal';
import TextField from '../../components/TextField';
//#endregion

//#region CONFIG IMPORTS
import { savePokemon, putErrorMessage } from '../../config/store/actions';
//#endregion

//#region UTILITY IMPORTS
import { getPokemonDetail } from '../../utils/api-service/pokemon';
import { colorizeByType } from '../../utils/helpers';
//#endregion


//#region STYLESHEET IMPORTS
import './PokemonDetail.scss';
import 'swiper/swiper.scss';
import 'swiper/components/effect-coverflow/effect-coverflow.scss';
//#endregion


SwiperCore.use([EffectCoverflow]);
const PokemonDetail = () => {
  //#region ROUTES
  const param = useParams();
  const id = param.id;
  //#endregion

  //#region REDUX
  const dispatch = useDispatch();
  const states = useSelector((state) => state);
  //#endregion

  //#region STATES
  const [characterData, setCharacterData] = useState(null);
  const [nickname, setNickname] = useState('');
  const [isModalOpen, setModalOpen] = useState(false);
  const [isCatched, setIsCatched] = useState(false);
  const [isFetching, setIsFetching] = useState(true);
  const [isSaved, setIsSaved] = useState(false);
  //#endregion

  //#region API HANDLER
  const fetchPokemonDetail = useCallback(async () => {
    try {
      const { data } = await getPokemonDetail(id);
      setCharacterData(data);
      setIsFetching(false);
    } catch (error) {}
  }, [id]);
  //#endregion

  //#region LIFECYCLES
  useEffect(() => {
    fetchPokemonDetail();
  }, [fetchPokemonDetail]);
  //#endregion

  //#region HANDLER
  const handleCatchCharacter = () => {
    const rand = Math.random();
    if (rand <= 0.5) {
      setIsCatched(true);
      setNickname(characterData.name);
      return;
    }

    setIsCatched(false);
  };

  const handleStoreCharacter = () => {
    const catchedData = { ...characterData, nickname: nickname.trim() };
    dispatch(savePokemon(catchedData));
    if (!states.error) setIsSaved(true);
  };

  const toggleModal = () => {
    setModalOpen(!isModalOpen);
    if (!isModalOpen) {
      handleCatchCharacter();
      return;
    }
    if (states.error) {
      dispatch(putErrorMessage(null));
    }

    setIsSaved(false);
    setNickname(characterData.name);
  };
  //#endregion

  //#region RENDERER
  const renderSwipeableImages = () => {
    return (
      <Swiper
        effect="coverflow"
        slidesPerView={2}
        coverflowEffect={{
          rotate: 50,
          stretch: 0,
          depth: 100,
          modifier: 1,
          slideShadows: false,
        }}
        grabCursor
        centeredSlides
      >
        <SwiperSlide
          className={isFetching ? 'is-fetching' : ''}
          style={{
            backgroundImage: isFetching
              ? ''
              : `url('${characterData.sprites.front_default}')`,
          }}
        ></SwiperSlide>
        <SwiperSlide
          className={isFetching ? 'is-fetching' : ''}
          style={{
            backgroundImage: isFetching
              ? ''
              : `url('${characterData.sprites.back_default}')`,
          }}
        ></SwiperSlide>
        <SwiperSlide
          className={isFetching ? 'is-fetching' : ''}
          style={{
            backgroundImage: isFetching
              ? ''
              : `url('${characterData.sprites.front_shiny}')`,
          }}
        ></SwiperSlide>
        <SwiperSlide
          className={isFetching ? 'is-fetching' : ''}
          style={{
            backgroundImage: isFetching
              ? ''
              : `url('${characterData.sprites.back_shiny}')`,
          }}
        ></SwiperSlide>
      </Swiper>
    );
  };

  const renderBottomSheet = () => {
    const isFetchingClasses = !isFetching ? '' : ' is-fetching';
    return (
      <BottomSheet>
        <div className="characterInfoSheet">
          <div className="infoSheetTitleContainer">
            <p className="infoSheetTitle inlineInfo">Height</p>
            <span className={isFetchingClasses}>
              {isFetching ? '' : `${characterData.height} dm`}
            </span>
          </div>
          <div className="infoSheetTitleContainer">
            <p className="infoSheetTitle inlineInfo">Weight</p>
            <span className={isFetchingClasses}>
              {isFetching ? '' : `${characterData.weight} Hg`}
            </span>
          </div>
          <p className="infoSheetTitle">Types</p>
          {isFetching && (
            <>
              <Chip className="characterChips is-fetching" />
              <Chip className="characterChips is-fetching" />
              <Chip className="characterChips is-fetching" />
              <Chip className="characterChips is-fetching" />
            </>
          )}
          {!isFetching &&
            characterData.types.map((item) => (
              <Chip
                key={item.type.name}
                className="characterChips"
                text={item.type.name}
                backgroundColor={colorizeByType(item.type.name)[0]}
                textColor={colorizeByType(item.type.name)[1]}
              />
            ))}
          <p className="infoSheetTitle">Abilities</p>
          {isFetching && (
            <>
              <Chip className="characterChips is-fetching" />
              <Chip className="characterChips is-fetching" />
              <Chip className="characterChips is-fetching" />
              <Chip className="characterChips is-fetching" />
            </>
          )}
          {!isFetching &&
            characterData.abilities.map((item) => (
              <Chip
                key={item.ability.name}
                className="characterChips"
                text={item.ability.name}
              />
            ))}
          <Button
            className="catchButton"
            disabled={isFetching}
            isRounded
            onClick={toggleModal}
          >
            Catch
          </Button>
        </div>
      </BottomSheet>
    );
  };

  const renderCharacterSavedModalContent = () => {
    return (
      <>
        <h3>{characterData.name} are saved successfully!</h3>
        <p>Nickname: {nickname}</p>
      </>
    );
  };

  const renderCharacterSavedFailedModalContent = () => {
    return (
      <>
        <h3>Failed to save!</h3>
        <p>{states.error}</p>
      </>
    );
  };

  const renderCatchSucceedModalContent = () => {
    return (
      <>
        <h3>Congratulation!🎉</h3>
        <TextField
          name="characterNickname"
          onChange={(e) => setNickname(e.target.value)}
          value={nickname}
          label="Give a nickname"
          placeholder="Nickname"
        />
        <div className="actionButtonContainer">
          <Button onClick={toggleModal}>Release</Button>
          <Button onClick={handleStoreCharacter}>Save</Button>
        </div>
      </>
    );
  };

  const renderCatchFailedModalContent = () => {
    return (
      <>
        <span className="failedModalCharacterName">{characterData.name}</span>
        <span> are too strong!</span>
        <p>Try again or Go catch another!</p>
      </>
    );
  };

  const renderModal = () => {
    if (!characterData) return null;
    return (
      <Modal
        className="nickNameInputModal"
        isOpen={isModalOpen}
        onClose={toggleModal}
      >
        {isCatched &&
          !isSaved &&
          !states.error &&
          renderCatchSucceedModalContent()}
        {!isCatched &&
          !isSaved &&
          !states.error &&
          renderCatchFailedModalContent()}
        {isCatched &&
          isSaved &&
          !states.error &&
          renderCharacterSavedModalContent()}
        {states.error && renderCharacterSavedFailedModalContent()}
      </Modal>
    );
  };
  //#endregion

  return (
    <div className="pokemonDetailContainer">
      <div className="characterHeaderContainer">
        <h3 className={`characterTitle${isFetching ? ' is-fetching' : ''}`}>
          {isFetching ? '' : characterData.name}
        </h3>
        <p className={`characterId${isFetching ? ' is-fetching' : ''}`}>
          {isFetching ? '' : `#${id}`}
        </p>
      </div>
      {renderSwipeableImages()}
      {renderBottomSheet()}
      {renderModal()}
    </div>
  );
};

export default PokemonDetail;
