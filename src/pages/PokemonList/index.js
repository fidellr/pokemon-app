//#region PACKAGE IMPORTS
import React, { useCallback, useEffect, useState } from 'react';
import { useSelector } from 'react-redux';
import { useHistory } from 'react-router-dom';
//#endregion

//#region MODULE IMPORTS
import Card from '../../components/Card';
import CardPlacholderShimmer from '../../components/Card/PlaceholderShimmer';
//#endregion

//#region UTILITY IMPORTS
import {
  fetchPokemons,
  getPokemonImage,
} from '../../utils/api-service/pokemon';
//#endregion

//#region STYLESHEET IMPORTS
import './PokemonList.scss';
//#endregion

const PokemonList = () => {
  //#region ROUTES
  const history = useHistory();
  //#endregion

  //#region REDUX
  const states = useSelector((state) => state);
  //#endregion

  //#region STATES
  const [pokemonList, setPokemonList] = useState(null);
  const [isLoading, setLoading] = useState(true);
  const [isLoadingMore, setIsLoadingMore] = useState(false);
  const [hasMore, setHasMore] = useState(true);
  const [fetchOffset, setFetchOffset] = useState(10);
  //#endregion

  //#region API HANDLER
  const fetchPokemonList = useCallback(async () => {
    try {
      if (isLoading) {
        const { data } = await fetchPokemons(10);
        if (!data.results.length || data.results.length < 10) setHasMore(false);
        setPokemonList(data.results);
        setLoading(false);
      }
    } catch (error) {
      console.log(error);
    }
  }, [isLoading]);

  const handleLoadMore = useCallback(async () => {
    const doc = document.documentElement;
    const scrollTop = doc.scrollTop || window.pageYOffset || 0;
    const scroll = window.innerHeight + scrollTop;
    const height = doc.offsetHeight;
    if (!hasMore) return;
    if (scroll >= height) {
      if (pokemonList) {
        setIsLoadingMore(true);
        setFetchOffset(fetchOffset + 10);
        const { data } = await fetchPokemons(10, fetchOffset);
        setPokemonList([...pokemonList, ...data.results]);
        setIsLoadingMore(false);
      }
    }
  }, [fetchOffset, hasMore, pokemonList]);
  //#endregion

  //#region LIFECYCLES
  useEffect(() => {
    fetchPokemonList();
  }, [fetchPokemonList]);

  useEffect(() => {
    window.addEventListener('scroll', handleLoadMore);
    return () => {
      window.removeEventListener('scroll', handleLoadMore);
    };
  }, [handleLoadMore]);
  //#endregion

  //#region RENDERER
  const renderPlaceholderShimmer = () => {
    return Array.from({ length: 6 }).map((_, i) => (
      <CardPlacholderShimmer key={i} className="pokemonCard" />
    ));
  };

  const renderPokemonCards = () => {
    if (isLoading) {
      return renderPlaceholderShimmer();
    }

    return pokemonList.map((item, i) => {
      const id = i + 1;
      return (
        <Card
          key={id}
          onClick={() => history.push(`/pokemon-detail/${id}`)}
          className="pokemonCard"
          name={item.name}
          id={id}
          image={getPokemonImage(id)}
        />
      );
    });
  };

  const renderSavedPokemonCount = () => {
    const isMyPokemonEmpty = !states.myPokemon || !states.myPokemon.length;
    return (
      <div className="myPokemonCount">
        <span>Saved Pokemon:</span>
        <span>
          <b>{isMyPokemonEmpty ? 0 : states.myPokemon.length}</b>
        </span>
      </div>
    );
  };
  //#endregion

  return (
    <>
      {renderSavedPokemonCount()}
      <div className="pokemonList">
        {renderPokemonCards()}
        {isLoadingMore && hasMore && renderPlaceholderShimmer()}
      </div>
    </>
  );
};

export default PokemonList;
