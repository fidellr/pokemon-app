//#region PACKAGE IMPORTS
import axios from 'axios';
//#endregion

//#region MODULE IMPORTS
import { POKEMON_URL } from './constants';
//#endregion

export const fetchPokemons = (limit, offset = 0) =>
  axios.get(`${POKEMON_URL}?limit=${limit}&offset=${offset}`);

export const getPokemonDetail = (id) => axios.get(`${POKEMON_URL}/${id}`);

export const getPokemonImage = (id) =>
  `https://raw.githubusercontent.com/PokeAPI/sprites/master/sprites/pokemon/${id}.png`;
