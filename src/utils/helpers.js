export const colorizeByType = (type) => {
  const colors = {
    normal: ['#A4ACAF', '#000'],
    fighting: ['#D66723', '#FFF'],
    flying: ['#3DC7EE', '#000'],
    poison: ['#B87EC8', '#000'],
    ground: ['#AA9742', '#000'],
    rock: ['#A38B21', '#FFF'],
    bug: ['#729F40', '#FFF'],
    ghost: ['#7C62A3', '#FFF'],
    steel: ['#9EB8B8', '#000'],
    fire: ['#FA7D24', '#FFF'],
    water: ['#4592C4', '#fff'],
    grass: ['#9CCC50', '#000'],
    electric: ['#EDD534', '#000'],
    psychic: ['#F265B9', '#FFF'],
    ice: ['#51C4E7', '#000'],
    dragon: ['#F16E57', '#FFF'],
    dark: ['#717171', '#FFF'],
    fairy: ['#FBB9E9', '#FFF'],
    unknown: ['#000000', '#FFF'],
    shadow: ['#5C5A56', '#000'],
  };

  return colors[type] || ['#bebebe', '#000'];
};

export const ls = {
  getItem: (key) => {
    const LStorage = window.localStorage;
    const item = LStorage.getItem(key);
    if (typeof item === 'string' && !item.length) return null;
    return JSON.parse(item);
  },
  setItem: (key, data) => {
    const LStorage = window.localStorage;
    LStorage.setItem(key, JSON.stringify(data));
  },
  removeItem: (key) => {
    const LStorage = window.localStorage;
    LStorage.removeItem(key);
  },
  flush: () => {
    const LStorage = window.localStorage;
    LStorage.clear();
  },
};
